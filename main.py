import asyncio

from command_executor import CommandExecutor, CommandErrorHandler
from command_parser import CommandParser
from config import Config
from application import Application
from echo_command_factory import EchoCommandFactory
from telegram_api import TelegramApi, Update
from listen_server import set_update_callback, flask_app

config = Config('config.json')

api = TelegramApi(config)
command_parser = CommandParser()
error_handler = CommandErrorHandler()
command_executor = CommandExecutor(error_handler)

yui = Application(api, command_parser, command_executor)

# set_update_callback(lambda update: api.on_update(Update(update)))
# api.register_update_handler(yui.process_update)

command_parser.register_command_factory('echo', EchoCommandFactory(api))
command_parser.register_command_factory('', EchoCommandFactory(api))


async def run_long_polling():
    offset = 0
    while True:
        print(offset)
        updates = await api.get_updates(offset, 10, 5, ['message'])
        for upd in updates:
            offset = max(offset, upd.update_id) + 1
            await yui.process_update(upd)


asyncio.run(run_long_polling())

if __name__ == '__main__':
    flask_app.run(debug=True, host='0.0.0.0')
