from command import Command
from command_factory import CommandFactory
from echo_command import EchoCommand
from telegram_api import Update, TelegramApi


class EchoCommandFactory(CommandFactory):

    def __init__(self, api: TelegramApi):
        self.api = api

    def create_cmd_from(self, upd: Update) -> Command:
        if upd.message is not None:
            return EchoCommand(upd.message.chat, upd.message.text, self.api)
        else:
            raise ValueError("Update doesn't contain a message to echo")
