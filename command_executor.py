from command import CommandResult, Command


class CommandErrorHandler:
    @staticmethod
    def handle(result: CommandResult):
        if result.is_error:
            print(result.exception)


class CommandExecutor:

    def __init__(self, error_handler: CommandErrorHandler):
        self.error_handler = error_handler

    async def execute(self, cmd: Command):
        res = await cmd.execute()
        if res.is_error:
            self.error_handler.handle(res)
