from command import Command, CommandResult
from telegram_api import TelegramApi, Chat


class EchoCommand(Command):
    def __init__(self, where: Chat, what: str, telegram_api: TelegramApi):
        self.api = telegram_api
        self.where = where
        self.what = what

    async def execute(self) -> CommandResult:
        await self.api.send_message(self.where, self.what)
        return CommandResult.make_success(None)
