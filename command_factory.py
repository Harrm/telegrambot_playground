from abc import ABCMeta, abstractmethod

from command import Command
from telegram_api import Update


class CommandFactory(metaclass=ABCMeta):

    @abstractmethod
    def create_cmd_from(self, upd: Update) -> Command:
        pass
