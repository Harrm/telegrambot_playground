from flask import Flask, request, make_response
from pprint import pprint

flask_app = Flask(__name__)


class _ListenServer:

    def __init__(self):
        self.update_callback = lambda _: _

    def set_update_callback(self, delegate):
        """ Expected to return status code """
        self.update_callback = delegate

    def on_update(self, update_json):
        return self.update_callback(update_json)


_listen_server = _ListenServer()


def set_update_callback(cb):
    """
    :param cb: a functor that takes a json object and returns HTTP status code
    """
    _listen_server.set_update_callback(cb)


@flask_app.route('/', methods=['GET'])
def hello():
    pprint(request.get_json())
    return make_response('Hello!')


@flask_app.route('/', methods=['POST'])
def update():
    pprint(request.get_json())
    response_code = _listen_server.on_update(request.get_json())
    return make_response('Pong', response_code)
