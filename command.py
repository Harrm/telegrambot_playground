from abc import ABCMeta, abstractmethod
from typing import Any


class CommandResult:

    def __init__(self, is_success: bool, *, value: Any = None, exception: Exception = None):
        self.is_success: bool = is_success
        self.exception: Exception = exception
        self.value: Any = value

    @staticmethod
    def make_success(value: Any):
        return CommandResult(True, value=value)

    @staticmethod
    def make_error(exception: Exception):
        return CommandResult(False, exception=exception)

    @property
    def is_error(self) -> bool:
        return not self.is_success


class Command(metaclass=ABCMeta):

    @abstractmethod
    async def execute(self) -> CommandResult:
        pass
