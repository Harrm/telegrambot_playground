from command_executor import CommandExecutor
from command_parser import CommandParser
from telegram_api import TelegramApi, Update


class Application:

    def __init__(self, api: TelegramApi, command_parser: CommandParser, command_executor: CommandExecutor):
        self.api = api
        self.command_parser = command_parser
        self.command_executor = command_executor

    async def setup_webhook(self):
        info = await self.api.get_webhook_info()
        if len(info.url) == 0:
            return await self.api.set_webhook(self.api.get_default_hook_url())

    async def process_update(self, event: Update):
        cmd = self.command_parser.parse_update_to_command(event)
        if cmd is not None:
            await self.command_executor.execute(cmd)
