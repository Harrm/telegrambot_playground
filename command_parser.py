from typing import Optional, Dict

from command import Command, CommandResult
from command_factory import CommandFactory
from telegram_api import Update


class CommandParser:

    def __init__(self):
        self.command_factories: Dict[str, CommandFactory] = {}

    def register_command_factory(self, name, command_factory):
        self.command_factories[name] = command_factory

    def parse_update_to_command(self, update: Update) -> Optional[Command]:
        if update.message is not None:
            cmd = self._extract_command(update.message.text)
            if cmd in self.command_factories:
                return self.command_factories[cmd].create_cmd_from(update)
            if cmd is None and '' in self.command_factories:
                return self.command_factories[''].create_cmd_from(update)
        return None

    @staticmethod
    def _extract_command(msg: str) -> Optional[str]:
        """
        A command is the part of the message starting with '/' and placed in the beginning.
        E. g. '/roll 1d8', 'roll is the command
        :param msg: the message supposed to contain a command
        :return:
        """
        if msg is None or len(msg) == 0:
            return None
        (firstWord, *_) = msg.split(maxsplit=1)
        if firstWord.startswith('/'):
            return firstWord[1:]
