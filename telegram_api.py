from typing import List, Optional

import requests
from config import Config


class TelegramFile:

    def __init__(self, param):
        self.param = param

    @staticmethod
    def create_with_id(file_id: str):
        return TelegramFile(file_id)

    @staticmethod
    def create_with_url(file_url: str):
        return TelegramFile(file_url)

    def to_method_param(self) -> str:
        return self.param


class WebhookInfo:
    def __init__(self, json_enc):
        self.info = json_enc

    @property
    def url(self) -> str:
        return self.info['url']


class User:
    def __init__(self, json_enc):
        self.json_enc = json_enc


class Chat:
    def __init__(self, json_enc):
        self.json_enc = json_enc

    @property
    def id(self) -> int:
        return self.json_enc['id']


class Message:
    def __init__(self, json_enc):
        self.json_enc = json_enc

    @property
    def text(self) -> Optional[str]:
        if 'text' in self.json_enc:
            return self.json_enc['text']
        return None

    @property
    def from_user(self) -> User:
        return User(self.json_enc['from'])

    @property
    def chat(self) -> Chat:
        return Chat(self.json_enc['chat'])


class InlineQuery:
    def __init__(self, json_enc):
        self.json_enc = json_enc

    @property
    def from_user(self) -> User:
        return User(self.json_enc['from'])

    @property
    def query(self) -> str:
        return self.json_enc['query']


class Update:
    def __init__(self, json_enc):
        self.json_enc = json_enc

    @property
    def update_id(self) -> int:
        return self.json_enc['update_id']

    @property
    def message(self) -> Optional[Message]:
        if 'message' in self.json_enc:
            return Message(self.json_enc['message'])
        return None

    @property
    def inline_query(self) -> Optional[Message]:
        if 'inline_query' in self.json_enc:
            return Message(self.json_enc['inline_query'])
        return None


class TelegramApi:

    def __init__(self, config: Config):
        self.config = config
        self.TELEGRAM_API = 'api.telegram.org'
        self.update_handler = lambda _: _

    def get_default_hook_url(self) -> str:
        return self.config.get_hook_url()

    async def set_webhook(self,
                          url: str,
                          certificate: TelegramFile = None,
                          max_connections: int = 40,
                          allowed_updates: List[str] = []):
        response = requests.post(self._get_method_url('setWebhook'),
                                 {'url': url,
                                  'certificate': certificate.to_method_param() if certificate else None,
                                  'max_connections': max_connections,
                                  'allowed_updates': allowed_updates})
        response.raise_for_status()

    async def get_webhook_info(self) -> WebhookInfo:
        response = requests.post(self._get_method_url('getWebhookInfo'))
        response.raise_for_status()
        return WebhookInfo(response.json())

    async def get_updates(self,
                          offset: int,
                          limit: int,
                          timeout: int,
                          allowed_updates: List[str] = []) -> List[Update]:
        response = requests.post(self._get_method_url('getUpdates'),
                                 {'offset': offset,
                                  'limit': limit,
                                  'timeout': timeout,
                                  'allowed_updates': allowed_updates
                                  })
        response.raise_for_status()
        response = response.json()['result']
        updates = []
        for upd in response:
            updates.append(Update(upd))
        return updates

    async def send_message(self, chat_where: Chat, text: str) -> Message:
        response = requests.post(self._get_method_url('sendMessage'),
                                 {'chat_id': chat_where.id,
                                  'text': text})
        response.raise_for_status()
        return Message(response.json())

    def on_update(self, update: Update):
        return self.update_handler(update) or requests.codes.ok

    def register_update_handler(self, handler):
        self.update_handler = handler

    def _get_method_url(self, method_name):
        return f"https://{self.TELEGRAM_API}/bot{self.config.get_token()}/{method_name}"
