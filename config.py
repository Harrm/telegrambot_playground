import json

class Config:

    def __init__(self, config_path):
        self.config = json.load(open(config_path, 'r'))

    def get_token(self) -> str:
        return self.config['token']

    def get_hook_url(self) -> str:
        return self.config['hook_url']